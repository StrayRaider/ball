import pygame
from yuvar_lib import settings, button, game

class LvlScreen():
    def __init__(self):
        #butonlar
        self.lvl = {1:((10,10),(150,100)),
                    2:((180,10),(150,100)),
                    3:((350,10),(150,100))}
        self.lvl_act =[1,2]
        self.buttons = []
        for i in self.lvl.keys():
            b = button.Button(self,self.lvl[i][0],settings.SCALE_FACTOR)
            b.set_text(str(i))
            b.set_fixed_size(self.lvl[i][1])
            b.set_function("change_lvl",[i])
            if int(b.get_text()) not in self.lvl_act:
                style = b.style
                style["free"] = (100,100,100)
                b.set_style(style)
            self.buttons.append(b)
    def update(self):
        pass
    def draw(self):
        settings.SCREEN.fill((0, 0, 0))
        for b in self.buttons:
            b.draw(settings.SCREEN)
    def mouse_but_down(self):
        for b in self.buttons:
            if int(b.get_text()) in self.lvl_act:
                b.state_control(pygame.mouse.get_pos(),True)
    def mouse_but_up(self):
        for b in self.buttons:
            if int(b.get_text()) in self.lvl_act:
                b.state_control(pygame.mouse.get_pos())
    def mouse_motion(self):
        for b in self.buttons:
            if int(b.get_text()) in self.lvl_act:
                b.state_control(pygame.mouse.get_pos())
    def key_down(self,key):
        pass
    def key_up(self,key):
        pass
    def change_lvl(self,lvl_c):
        settings.active_screen = game.GameScreen()
        settings.active_screen.set_lvl(lvl_c)

