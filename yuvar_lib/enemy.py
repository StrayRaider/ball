import pygame
import os
import random
from yuvar_lib import settings

class Enemy():
    def __init__(self, location):
        self.spawn_loc = list((int(location[0]),int(location[1])))
        self.location = list((int(location[0]),int(location[1])))
        self.size = 150
        self.radius = self.size / 2
        self.center = [self.location[0] + self.radius, self.location[1] + self.radius]
        self.image = pygame.image.load(os.path.join("assets", "enemy_drw.png"))
        self.gravity = 2
        self.vector = [0, 0]
        self.velocity = 3
        self.max_gravity_f = 20
        self.touching = []
        self.direction()


    def draw(self):
        settings.SCREEN.blit(self.image,self.location)

    def restart(self):
        print(self.spawn_loc)
        self.location = list((int(self.spawn_loc[0]),int(self.spawn_loc[1])))

    def kill_self(self):
        del self

    def direction(self):
        if "right" in self.touching:
            self.vector[0] = -self.velocity
        elif "left" in self.touching:
            self.vector[0] = self.velocity
        else:
            if random.randint(0, 1):
                self.vector[0] = -self.velocity
            else:
                self.vector[0] = self.velocity

    def update(self):
        if self.vector[1] < self.max_gravity_f:
            self.vector[1] += self.gravity

        self.location[0] += self.vector[0]
        self.location[1] += self.vector[1]
        self.center = [self.location[0] + self.radius, self.location[1] + self.radius]

    def is_crush(self, obj_list):
        self.touching.clear()
        if self.vector[0] == 0 and self.vector[1] == 0:
            pass
        else:
            for obj in obj_list:
                # x i kutu objesinin sağı ve solu aralığında ise
                if obj.location[1][0] + self.radius / 2 >= self.center[0] >= obj.location[0][0] - self.radius / 2:
                    if obj.location[0][1] > self.center[1]:
                        if obj.location[0][1] - self.center[1] <= self.radius:
                            self.vector[1] = 0
                            distance = self.radius - (obj.location[0][1] - self.center[1])
                            self.location[1] -= distance

                if obj.location[1][1] + self.radius / 2 >= self.center[1] >= obj.location[0][1] - self.radius / 2:

                    if obj.location[0][0] > self.center[0]:
                        if obj.location[0][0] - self.center[0] < self.radius:
                            self.vector[0] = 0
                            self.touching.append("right")
                            self.direction()

                    elif obj.location[0][0] < self.center[0]:  # karakter sagdan gelir
                        if self.center[0] - obj.location[1][0] < self.radius:
                            self.vector[0] = 0
                            self.touching.append("left")
                            self.direction()

