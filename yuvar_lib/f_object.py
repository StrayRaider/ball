import pygame
from yuvar_lib import settings

class F_Obj():
    def __init__ (self,loc,size,img):
        self.old_loc = loc
        self.size = size
        self.location = [[loc[0],loc[1]],[loc[0]+size[0],loc[1]+size[1]]]
        self.image = pygame.image.load(img)
        self.image = pygame.transform.scale(self.image, self.size)
    def draw(self):
        settings.SCREEN.blit(self.image,self.location[0])

    def restart(self):
        self.location = [[self.old_loc[0],self.old_loc[1]],[self.old_loc[0]+self.size[0],self.old_loc[1]+self.size[1]]]

