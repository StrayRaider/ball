import pygame
from yuvar_lib import settings

class C_Obj():
    def __init__ (self,loc,size,img_on,img_off):
        self.old_loc = loc
        self.size = size
        self.location = [[loc[0],loc[1]],[loc[0]+size[0],loc[1]+size[1]]]
        self.img_on = img_on
        self.img_off = img_off
        self.image = pygame.image.load(self.img_off)
        self.collected = False
    def draw(self):
        settings.SCREEN.blit(self.image,self.location[0])

    def detect(self,boolen):
        if boolen:
            self.collected = True
            self.image = pygame.image.load(self.img_on)
    def restart(self):
        self.location = [[self.old_loc[0],self.old_loc[1]],[self.old_loc[0]+self.size[0],self.old_loc[1]+self.size[1]]]
        self.collected = False
        self.image = pygame.image.load(self.img_off)
