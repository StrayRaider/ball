import pygame
import math
from yuvar_lib import settings

class F_Obj_ang():
    def __init__ (self,center, angle, size, img):
        self.old_center = center
        self.size = size
        self.center = center
        self.angle = angle
        self.location = [[center[0]-(self.size[0]/2)/math.cos(self.angle),center[1]-(self.size[1]/2)/math.cos(self.angle)],
                        [center[0]+(self.size[0]/2)/math.cos(self.angle),center[1]+(self.size[1]/2)/math.cos(self.angle)]]
        self.image = pygame.image.load(img)
        self.image = pygame.transform.scale(self.image, self.size)

        self.r_image = None
        self.d_location = []
        self.rotate()

    def draw(self):
        settings.SCREEN.blit(self.r_image,self.center)

    def update(self):
        pass
        #self.location = [[self.center[0],self.location[0][1]],[self.location[1][0]+self.size[0],self.location[1][1]+self.size[1]]]

    def rotate(self):
        self.r_image = pygame.transform.rotate(self.image,self.angle)
        self.d_location = self.image.get_rect(center=(self.center))

    def restart(self):
        self.center = [[self.old_center[0],self.old_center[1]],[self.old_center[0]+self.size[0],self.old_center[1]+self.size[1]]]

    def get_y(self,character_center):
        if self.angle != 0:
            y = abs(math.tan(self.angle)) * abs(self.center[0] - character_center[0])
            #karakter sağda
            if 0 > self.center[0] - character_center[0]:
                print(self.center[1] - y - (math.cos(self.angle)*self.size[1])/2)
                return self.center[1] - y - (math.cos(self.angle)*self.size[1])/2
            else:
                print(self.center[1] + y - (math.cos(self.angle)*self.size[1])/2)
                return self.center[1] + y - (math.cos(self.angle)*self.size[1])/2

