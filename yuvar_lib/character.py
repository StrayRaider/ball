import pygame, os, math
from yuvar_lib import settings


class Character():
    def __init__ (self):
        self.location = [250,200]
        self.d_location = []
        self.r_image = None
        self.size = 150
        self.radius = self.size/2
        self.center = [self.location[0]+self.radius, self.location[1]+self.radius]
        self.img_center = [self.center[0],self.center[1]]
        self.image =pygame.image.load(os.path.join("assets","ball_2.png"))
        self.image = pygame.transform.scale(self.image, (self.size,self.size))
        self.gravity = 10
        self.vector = [0,0]
        self.velocity = 8
        self.keys = []
        self.move_f = [1,1]
        self.max_gravity_f = 20
        self.touching = []
        self.angle = 0

    def draw(self):
        self.rot_center()
        settings.SCREEN.blit(self.r_image,self.d_location)


    def rot_center(self):
        self.r_image = pygame.transform.rotate(self.image,self.angle)
        self.d_location = self.r_image.get_rect(center=(self.center))


    def update(self):

        if "right" not in self.keys and "left" not in self.keys:
            if self.vector[0] > 0:
                self.vector[0] -= self.velocity/16
                self.angle -= self.vector[0]

            elif self.vector[0] < 0:
                self.vector[0] += self.velocity/16
                self.angle -= self.vector[0]

        elif "right" in self.keys:
            self.vector[0] = self.velocity
            self.angle -= self.vector[0]
        elif "left" in self.keys:
            self.vector[0] = -self.velocity
            self.angle -= self.vector[0]

        if self.vector[1] < self.max_gravity_f:
            self.vector[1] += 2

        self.location[0] += self.vector[0] * self.move_f[0]
        self.location[1] += self.vector[1] * self.move_f[1]
        self.center = [self.location[0]+self.radius, self.location[1]+self.radius]

    def is_angled(self,obj_list):
        if self.vector[0] == 0  and self.vector[1] == 0:
            pass
        else:
            for obj in obj_list:
                y_loc = obj.get_y(self.center)
                if obj.location[1][0] >= self.center[0] >= obj.location[0][0]:
                    if y_loc > self.center[1]:
                        if y_loc - self.center[1] <= self.radius:
                            if "up" not in self.keys:
                                self.vector[1] = 0
                            else:
                                self.vector[1] = -self.velocity*4
                            distance = self.radius - (y_loc - self.center[1])
                            #self.vector[1] -= distance*0.1
                            self.location[1] -= distance

                    elif obj.location[1][1] < self.center[1]:
                        if abs(obj.location[1][1] - self.center[1]) <= self.radius:
                            self.vector[1] = 0

    def is_crush(self,obj_list):
        self.touching.clear()
        if self.vector[0] == 0  and self.vector[1] == 0:
            pass
        else:
            for obj in obj_list:
                # x i kutu objesinin sağı ve solu aralığında ise
                if obj.location[1][0] + self.radius/2 >= self.center[0] >= obj.location[0][0] - self.radius/2:
                    if obj.location[0][1] > self.center[1]:
                        if obj.location[0][1] - self.center[1] <= self.radius:
                            if "up" not in self.keys:
                                self.vector[1] = 0
                            else:
                                self.vector[1] = -self.velocity*4
                            distance = self.radius - (obj.location[0][1] - self.center[1])
                            #self.vector[1] -= distance*0.1
                            self.location[1] -= distance

                    elif obj.location[1][1] < self.center[1]:
                        if abs(obj.location[1][1] - self.center[1]) <= self.radius:
                            self.vector[1] = 0

    
                if obj.location[1][1] + self.radius/2 >= self.center[1] >= obj.location[0][1] - self.radius/2:
                    
                    if obj.location[0][0] > self.center[0]: # karakter soldan gelir
                        if obj.location[0][0] - self.center[0] < self.radius:
                            if "left" not in self.keys:
                                self.vector[0] = 0
                                self.touching.append("right")
                            distance = self.radius - (obj.location[0][0] - self.center[0])
                            self.location[0] -= distance
    
                    elif obj.location[0][0] < self.center[0]: # karakter sagdan gelir
                        if self.center[0] - obj.location[1][0] < self.radius:
                            if "right" not in self.keys:
                                self.vector[0] = 0
                                self.touching.append("left")

                            distance = self.radius + (obj.location[1][0] - self.center[0])
                            self.location[0] += distance


    def is_touch(self,obj_list):
        for obj in obj_list:
            if obj.location[1][0]+self.radius >= self.center[0] >= obj.location[0][0]-self.radius:
                if obj.location[1][1]+self.radius >= self.center[1] >= obj.location[0][1]-self.radius:
                    if not obj.collected:
                        obj.detect(True)
                        return 1

    def is_enemy(self,enemys):
        for enemy in enemys:
            if enemy.center[0]+enemy.size>= self.center[0] >= enemy.center[0]-enemy.size:
                if enemy.center[1] > self.center[1]:
                    if enemy.center[1] - self.center[1] <= self.radius:
                        return enemy
            if enemy.location[1]+enemy.size >= self.center[1] >= enemy.location[1]:
                if enemy.location[0] > self.center[0]: # karakter soldan gelir
                    if enemy.location[0] - self.center[0] < self.radius:
                        return True
                elif enemy.location[0] < self.center[0]: # karakter sagdan gelir
                    if self.center[0] - enemy.location[0] -enemy.size < self.radius:
                        return True


