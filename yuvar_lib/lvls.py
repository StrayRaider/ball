import os
from yuvar_lib import f_object, n_object, c_object,enemy, f_object_angled

img_1 = os.path.join("assets","rect_1.png")
img_2 = os.path.join("assets","ground.png")

img_on = os.path.join("assets","star_on.png")
img_off = os.path.join("assets","star_off.png")

lvl_d = {

        "sablon":{"f_objects":[],

            "c_objects":[],

            "n_objects":[],

            "f_obj_ang":[],

            "enemy":[],

            "star_c":[]},



         1:{"f_objects":[f_object.F_Obj((0,100),(100,400), img_2),
                         f_object.F_Obj((100,200),(100,300), img_2),
                         f_object.F_Obj((0,500),(1000,100), img_2),
                         f_object.F_Obj((1000,400),(100,100), img_2),
                         f_object.F_Obj((1000,500),(1000,100), img_2),
                         f_object.F_Obj((1900,400),(100,100), img_2),
                         f_object.F_Obj((1900,600),(500,100), img_2),
                         f_object.F_Obj((2300,500),(1000,100), img_2),
                         f_object.F_Obj((3200,100),(100,400), img_2),],

            "c_objects":[c_object.C_Obj((980,200),(100,100),img_on,img_off),
                         c_object.C_Obj((2080,400),(100,100),img_on,img_off),
                         c_object.C_Obj((3000,300),(100,100),img_on,img_off)],

            "n_objects":[],

            "f_obj_ang":[f_object_angled.F_Obj_ang([500,200],1,[200,100],img_2)],

            "enemy":[enemy.Enemy([1400,200])],

            "star_c":[0,3]},


         2:{"f_objects":[f_object.F_Obj((0,100),(100,1000), img_2),
                         f_object.F_Obj((0,100),(400,100), img_2),
                         f_object.F_Obj((0,500),(1000,100), img_2),
                         f_object.F_Obj((900,600),(400,100), img_2),
                         f_object.F_Obj((1200,700),(400,100), img_2),
                         f_object.F_Obj((1500,800),(500,100), img_2),
                         f_object.F_Obj((1900,700),(400,100), img_2),
                         f_object.F_Obj((2200,600),(400,100), img_2),
                         f_object.F_Obj((2600,600),(500,100), img_2),
                         f_object.F_Obj((3000,700),(100,500), img_2),
                         f_object.F_Obj((3400,300),(100,900), img_2),
                         f_object.F_Obj((3400,1100),(400,100), img_2),
                         f_object.F_Obj((3700,1200),(100,300), img_2),
                         f_object.F_Obj((2700,1500),(1100,100), img_2),
                         f_object.F_Obj((2700,1300),(100,200), img_2),
                         f_object.F_Obj((2300,1300),(500,100), img_2),
                         f_object.F_Obj((1900,1200),(500,100), img_2),
                         f_object.F_Obj((1000,1100),(1000,100), img_2),
                         f_object.F_Obj((0,1100),(1000,100), img_2)],

            "c_objects":[c_object.C_Obj((980,200),(100,100),img_on,img_off),
                         c_object.C_Obj((2080,400),(100,100),img_on,img_off),
                         c_object.C_Obj((200,800),(100,100),img_on,img_off)],

            "n_objects":[],

            "f_obj_ang":[],

            "enemy":[],

            "star_c":[0,3]}
        }
