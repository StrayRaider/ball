import pygame
import time
import math
from yuvar_lib import settings, button, lvl_select_screen, character, lvls

class GameScreen():
    def __init__(self):
        self.buttons = []
        exit_button = button.Button(self,[1000,100],1)
        exit_button.set_text("exit")
        exit_button.set_function("exit")
        back_button = button.Button(self,[1150,100],1)
        back_button.set_text("restart")
        back_button.set_function("restart")

        self.buttons.append(exit_button)
        self.buttons.append(back_button)

        self.character = character.Character()
        self.active_lvl = None
        settings.camera_loc = self.character.center
        settings.camera_center_loc = [550,350]
        
    def restart(self):
        for obj in self.objects["f_objects"]:
            obj.restart()
        for obj in self.objects["c_objects"]:
            obj.restart()
        for obj in self.objects["enemy"]:
            obj.restart()
        self.objects["star_c"][0] = 0
        self.go_back()
            
    def update(self):
        if self.star_count[0] >= self.star_count[1]:
            self.set_lvl(int(self.active_lvl)+1)

        self.character.is_crush(self.objects["f_objects"])

        self.character.is_angled(self.objects["f_obj_ang"])

        for enemy in self.objects["enemy"]:
            enemy.is_crush(self.objects["f_objects"])
            enemy.update()
        for angled in self.objects["f_obj_ang"]:
            angled.update()

        if self.character.is_touch(self.objects["c_objects"]):
            self.star_count[0] += 1
        enemy_is_c = self.character.is_enemy(self.objects["enemy"])
        if enemy_is_c != None and enemy_is_c == True:
            print("character.dead")
            self.restart()
        if enemy_is_c != None and enemy_is_c != True:
            enemy_is_c.kill_self()
            self.objects["enemy"].remove(enemy_is_c)
        try:
            self.change_map()
        except:
            print("map did not changed")
        self.character.update()



    def draw(self):
        settings.SCREEN.fill((10, 10, 10))

        self.character.draw()

        for obj in self.objects["f_objects"]:
            obj.draw()
        for obj in self.objects["c_objects"]:
            obj.draw()
        for obj in self.objects["enemy"]:
            obj.draw()
        for obj in self.objects["f_obj_ang"]:
            obj.draw()

        for b in self.buttons:
            b.draw(settings.SCREEN)

    def update_cam(self):
        settings.camera_loc = self.character.center

    def change_map(self):
        self.update_cam()

        dist_y = (settings.camera_center_loc[1] - settings.camera_loc[1])/100
        if abs(settings.camera_center_loc[1]-self.character.center[1]) <= 100:
            if "up" in self.character.keys:
                self.character.move_f[1] = 1
            dist_y = 0
        else :
            if settings.camera_center_loc[1]-self.character.center[1] < 0:
                dist_y = (abs(settings.camera_center_loc[1]-self.character.center[1])-100)*0.1
                self.character.location[1] -= dist_y
                for obj in self.objects["f_objects"]:
                    obj.location[0][1] -= dist_y
                    obj.location[1][1] -= dist_y
                for obj in self.objects["c_objects"]:
                    obj.location[0][1] -= dist_y
                    obj.location[1][1] -= dist_y
                for obj in self.objects["enemy"]:
                    obj.location[1] -= dist_y
                for obj in self.objects["f_obj_ang"]:
                    obj.center[1] -= dist_y

            elif settings.camera_center_loc[1]-self.character.center[1] > 0:
                dist_y = (abs(settings.camera_center_loc[1]-self.character.center[1])-100)*0.1
                self.character.location[1] += dist_y
                for obj in self.objects["f_objects"]:
                    obj.location[0][1] += dist_y
                    obj.location[1][1] += dist_y
                for obj in self.objects["c_objects"]:
                    obj.location[0][1] += dist_y
                    obj.location[1][1] += dist_y
                for obj in self.objects["enemy"]:
                    obj.location[1] += dist_y
                for obj in self.objects["f_obj_ang"]:
                    obj.center[1] += dist_y

            self.character.move_f[1] = 1
            dist_y = 0

        if abs(settings.camera_center_loc[0]-self.character.center[0]) <= 300:
            dist_x = (settings.camera_center_loc[0] - settings.camera_loc[0])/300
            #distance_x 0 dan büyükse karakter solda
            if "right" in self.character.keys and dist_x >= 0:
                self.character.move_f[0] = 1
            elif "left" in self.character.keys and dist_x <= 0:
                self.character.move_f[0] = 1
            else:
                self.character.move_f[0] = 1-abs(dist_x)
        else :
            if settings.camera_center_loc[0]-self.character.center[0] < 0:
                dist_x = abs(settings.camera_center_loc[0]-self.character.center[0])-300
                self.character.location[0] -= dist_x
                for obj in self.objects["f_objects"]:
                    obj.location[0][0] -= dist_x
                    obj.location[1][0] -= dist_x
                for obj in self.objects["c_objects"]:
                    obj.location[0][0] -= dist_x
                    obj.location[1][0] -= dist_x
                for obj in self.objects["enemy"]:
                    obj.location[0] -= dist_x
                for obj in self.objects["f_obj_ang"]:
                    obj.center[0] -= dist_x

            elif settings.camera_center_loc[0]-self.character.center[0] > 0:
                dist = abs(settings.camera_center_loc[0]-self.character.center[0])-300
                self.character.location[0] += dist
                for obj in self.objects["f_objects"]:
                    obj.location[0][0] += dist_x
                    obj.location[1][0] += dist_x
                for obj in self.objects["c_objects"]:
                    obj.location[0][0] += dist_x
                    obj.location[1][0] += dist_x
                for obj in self.objects["enemy"]:
                    obj.location[0] += dist_x
                for obj in self.objects["f_obj_ang"]:
                    obj.center[0] += dist_x

            self.character.move_f[0] = 1
            dist_x = 0

        for obj in self.objects["f_objects"]:
            if "left" not in self.character.touching and "right" not in self.character.touching:
                obj.location[0][0] -= self.character.vector[0]*abs(dist_x)
                obj.location[1][0] -= self.character.vector[0]*abs(dist_x)
                obj.location[0][1] -= self.character.vector[1]*abs(dist_y)
                obj.location[1][1] -= self.character.vector[1]*abs(dist_y)

        for obj in self.objects["c_objects"]:
            if "left" not in self.character.touching and "right" not in self.character.touching:
                obj.location[0][0] -= self.character.vector[0]*abs(dist_x)
                obj.location[1][0] -= self.character.vector[0]*abs(dist_x)
                obj.location[0][1] -= self.character.vector[1]*abs(dist_y)
                obj.location[1][1] -= self.character.vector[1]*abs(dist_y)

        for obj in self.objects["enemy"]:
            if "left" not in self.character.touching and "right" not in self.character.touching:
                obj.location[0] -= self.character.vector[0]*abs(dist_x)
                obj.location[1] -= self.character.vector[1]*abs(dist_y)

        for obj in self.objects["f_obj_ang"]:
            if "left" not in self.character.touching and "right" not in self.character.touching:
                obj.center[0] -= self.character.vector[0]*abs(dist_x)
                obj.center[1] -= self.character.vector[1]*abs(dist_y)

    def mouse_but_down(self):
        for b in self.buttons:
            b.state_control(pygame.mouse.get_pos(),True)

    def mouse_but_up(self):
        for b in self.buttons:
            b.state_control(pygame.mouse.get_pos())

    def mouse_motion(self):
        for b in self.buttons:
            b.state_control(pygame.mouse.get_pos())

    def key_down(self,key):

        if key == pygame.K_UP:
            if "up" not in self.character.keys:
                self.character.keys.append("up")

        elif key == pygame.K_DOWN:
            if "down" not in self.character.keys:
                self.character.keys.append("down")

        elif key == pygame.K_RIGHT:
            if "right" not in self.character.keys:
                self.character.keys.append("right")

        elif key == pygame.K_LEFT:
            if "left" not in self.character.keys:
                self.character.keys.append("left")

    def key_up(self,key):
        try:
            if key == pygame.K_UP:
                self.character.keys.remove("up")
            elif key == pygame.K_DOWN:
                self.character.keys.remove("down")
            elif key == pygame.K_RIGHT:
                self.character.keys.remove("right")
            elif key == pygame.K_LEFT:
                self.character.keys.remove("left")
        except:
            pass

    def set_lvl(self,lvl):
        self.active_lvl = lvl
        self.objects = lvls.lvl_d[self.active_lvl]
        self.star_count = self.objects["star_c"]

    def exit(self):
        pygame.quit()

    def go_back(self):
        settings.active_screen = lvl_select_screen.LvlScreen()        

