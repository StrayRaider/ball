import pygame, sys
from yuvar_lib import settings, lvl_select_screen

def get_screen_info():
    s_info = pygame.display.Info()
    s_width = s_info.current_w
    s_height = s_info.current_h
    # Ekran Genişliği 16 ya 9 mu anlayalım
    if s_width/s_height == 16/9:
        settings.SCALE_FACTOR = s_width/1920
        settings.SCREEN_WIDTH = s_width
        settings.SCREEN_HEIGHT = s_height
    else:
        x_scale_factor = s_width / 1920
        y_scale_factor = s_height / 1080
        if x_scale_factor >= y_scale_factor:
            settings.SCALE_FACTOR = y_scale_factor
            settings.SCREEN_WIDTH = int(1920*settings.SCALE_FACTOR)
            settings.SCREEN_HEIGHT = s_height
        else:
            settings.SCALE_FACTOR = x_scale_factor
            settings.SCREEN_WIDTH = s_width
            settings.SCREEN_HEIGHT = int(1080*settings.SCALE_FACTOR)                          

def read_settings():
    pass


def write_settings():
    pass

#Ayarları okuyalım
read_settings()
pygame.init()
get_screen_info()
FPSCLOCK = pygame.time.Clock()
settings.SCREEN = pygame.display.set_mode((settings.SCREEN_WIDTH,settings.SCREEN_HEIGHT))
settings.camera = [[0,0],[settings.SCREEN_WIDTH,settings.SCREEN_HEIGHT]]

settings.active_screen = lvl_select_screen.LvlScreen()

while True:
    #tuş eventleri
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            pygame.quit()
            write_settings()
            sys.exit()
        elif event.type == pygame.MOUSEBUTTONDOWN:
            settings.active_screen.mouse_but_down()
        elif event.type == pygame.MOUSEBUTTONUP:
            settings.active_screen.mouse_but_up()
        elif event.type == pygame.MOUSEMOTION:
            settings.active_screen.mouse_motion()

        elif event.type == pygame.KEYDOWN:
            settings.active_screen.key_down(event.key)
        elif event.type == pygame.KEYUP:
            settings.active_screen.key_up(event.key)


    settings.active_screen.update()
    settings.active_screen.draw()
    pygame.display.flip()
    FPSCLOCK.tick(settings.FPS)
